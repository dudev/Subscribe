<?php

namespace app\controllers;

use app\extensions\Controller;
use app\models\AuthKey;
use app\models\User;
use general\ext\api\auth\AuthApi;
use general\ext\api\auth\AuthUrlCreator;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class SiteController extends Controller {
    //public $defaultAction = 'login';
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['signup', 'index'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	//@todo: закоментировать
    public function actionIndex() {
        return $this->render('index');
    }

	/*public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}*/

    public function actionLoginWithAuthKey($result) {
	    $this->layout = 'emptyHtml';
	    if($result == 'success') {
		    if (!Yii::$app->user->isGuest) {
			    return $this->render('redirect', ['url' => '/']);
		    }

		    $mark = Yii::$app->request->get('key');
		    $model = $user = null;
		    if($key = AuthApi::userGetServiceAuthKey($mark)) {
			    $model = new AuthKey();
			    $model->auth_key = $key['auth_key'];
			    $model->browser = Yii::$app->request->userAgent;
			    $model->ip = Yii::$app->request->userIP;
			    $model->user_id = $key['user_id'];
			    $model->save();
		    }
		    if($model) {
			    $user = $model->user ?: User::getFromAuth($model->user_id);
		    }
		    if ($user
			&& Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
			    return $this->render('refresh');
		    }
	    }
	    return $this->render('redirect', ['url' => '/']);
    }

	public function actionLoginWithMark($mark) {
		if (!Yii::$app->user->isGuest) {
			return '';
		}

		\Yii::$app->response->format = Response::FORMAT_JSONP;
		$model = $user = null;
		if($key = AuthApi::userGetServiceAuthKey($mark)) {
			$model = new AuthKey();
			$model->auth_key = $key['auth_key'];
			$model->browser = Yii::$app->request->userAgent;
			$model->ip = Yii::$app->request->userIP;
			$model->user_id = $key['user_id'];
			$model->save();
		}
		if($model) {
			$user = $model->user ?: User::getFromAuth($model->user_id);
		}
		if ($user
			&& Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
			return [
				'data' => 'success',
				'callback' => 'loginWithMark',
			];
		}
		return [
			'data' => 'error',
			'callback' => 'loginWithMark',
		];
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		return $this->render('login');
	}

    public function actionLogout() {
        User::logout();
	    return $this->redirect(
		    AuthUrlCreator::userLogout(
			    Yii::$app->id,
			    '//' . Yii::$app->request->getServerName()));
    }
	public function actionLogoutEverywhere() {
		User::logoutEverywhere();
		return $this->redirect(
			AuthUrlCreator::userLogoutEverywhere(
				Yii::$app->id,
				'//' . Yii::$app->request->getServerName()));
	}
}