<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;


use app\models\Site;

/**
 * Class Controller
 * @property Site $site
 */
class Controller extends \yii\web\Controller {

} 