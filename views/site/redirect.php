<?php
/**
 * Project: Blog Platform - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use general\controllers\api\Controller;

/* @var $url string */
?>
<script type="text/javascript">
parent.location.href= "<?= $url ?>";
</script>